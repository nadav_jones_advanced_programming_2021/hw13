#pragma once

class ClientHandler;

#include "pch.h"
#include "ClientHandler.h"
#include <map>
#include <deque>
#include <mutex>
#include <condition_variable>

#define CLIENT_PORT 8876
#define TESTER_PORT 8826

class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	std::string readChatFile(const std::string& filename);
	void addMsg(const std::string& msg);
	void notifyMsg();
	std::string getAllUsers();
	void logout(const std::string& username);


private:

	void accept();
	void writeChatFile(const std::string& filename, std::string msg);
	void handleQueue();

	SOCKET _serverSocket;
	std::map<std::string, ClientHandler*> _connected_users;
	std::map<std::string, std::mutex> _chats_mu;
	std::deque<std::string> _msg_queue;
	std::condition_variable _pending_msg;
	std::mutex _queue_mu;
	std::thread _msg_handler;
	
};
