#include "Server.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <fstream>
#include <string>

/*
CTOR for server object
INPUT: None
OUTPUT: None
*/

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	this->_msg_handler = std::thread(&Server::handleQueue, this);
	
}

/*
DTOR for server object
INPUT: None
OUTPUT: None
*/

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}

	this->_msg_handler.detach();

}

/*
Creates listening socket for incoming clients and accepts connection requests
INPUT: None
OUTPUT: None
*/

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
		//Helper::clearDisconnectedUsers(this->_connected_users);
	}
}

/*
Reads contents of chat log file
INPUT: file name, constant string reference
OUTPUT: file contents, string
*/

std::string Server::readChatFile(const std::string& file_name)
{
	std::string ret = "";

	if (this->_chats_mu.find(file_name) == this->_chats_mu.end())
	{
		return ret;
	}

	std::ifstream file;

	std::unique_lock<std::mutex> chatLock(this->_chats_mu[file_name]);
	file.open(file_name, std::ifstream::binary);

	if (file.is_open())
	{
		
		unsigned int end = 0;
		char* content = nullptr;

		file.seekg(0, std::ios::end);
		end = file.tellg();
		file.seekg(0, std::ios::beg);
		content = new char[end + 1];
		file.read(content, end);
		content[end] = 0;
		ret = std::string(content);
		delete[] content;
		file.close();
		chatLock.unlock();
		return ret;

	}
	else
	{
		chatLock.unlock();
		throw std::string("Error opening file for reading\n");
		return 0;
	}
}

/*
Writes a new message to a chat log file
INPUT: file name and new message, 2x string
OUTPUT: None
*/

void Server::writeChatFile(const std::string& filename, std::string msg)
{
	std::ofstream file;

	std::unique_lock<std::mutex> chatLock(this->_chats_mu[filename]);
	file.open(filename, std::ios::app);
	if (file.is_open())
	{
		file << msg;
		chatLock.unlock();
		file.close();
	}
	else
	{
		chatLock.unlock();
		throw std::string("Error opening file for writing\n");
	}

}

/*
Handles new messages recieved from clients in message queue
INPUT: None
OUTPUT: None
*/

void Server::handleQueue()
{
	std::string msg;
	const std::string msg_format = "&MAGSH_MESSAGE&&Author&";

	while (true)
	{
		std::unique_lock<std::mutex> queueLock(this->_queue_mu);
		this->_pending_msg.wait(queueLock);

		while (!this->_msg_queue.empty())
		{
			msg = this->_msg_queue.back();
			this->_msg_queue.pop_back();
			queueLock.unlock();

			this->writeChatFile(msg.substr(0, msg.find("#")), msg_format + msg.substr(msg.find("#")+1, (msg.find("##") - msg.find("#")-1)) + "&DATA&" + msg.substr(msg.find("##") + 2));
			queueLock.lock();
		}


	}

}

/*
Adds a new message to the message queue
INPUT: new message, constanst string reference
OUTPUT: None
*/

void Server::addMsg(const std::string& msg)
{
	std::unique_lock<std::mutex> queueLock(this->_queue_mu);
	this->_msg_queue.push_back(msg);
	queueLock.unlock();

}

/*
Notifies message handler of new messages in queue
INPUT: None
OUTPUT: None
*/

void Server::notifyMsg()
{
	this->_pending_msg.notify_one();
}

/*
Gets all currently connected users
INPUT: None
OUTPUT: All currently connected users, string
*/

std::string Server::getAllUsers() 
{
	std::string userList;

	for (std::map<std::string, ClientHandler*>::iterator iter = this->_connected_users.begin(); iter != this->_connected_users.end(); iter++)
	{
		if (iter != this->_connected_users.begin())
		{
			userList += "&";
		}
		userList += iter->first;
		
	}

	return userList;
}

/*
Removes disconnected user from client map
INPUT: Client username, constanst string reference
OUTPUT: None
*/

void Server::logout(const std::string& username)
{
	this->_connected_users.erase(username);
}

/*
Accepts a new connection request, parses client's username and creates thread for the new client
INPUT: None
OUTPUT: None
*/

void Server::accept()
{

	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);
	int len = 0;
	std::string name;
	

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	try
	{
		len = Helper::getIntPartFromSocket(client_socket, 5) - 20000;
		name = Helper::getStringPartFromSocket(client_socket, len);
		this->_connected_users[name] = new ClientHandler(client_socket, name, *this);
	}
	catch (std::exception& e)
	{
		std::cout << "Undefined behaviour from accepted socket before logged in: " << e.what() << std::endl;
	}
	
	
	

}


