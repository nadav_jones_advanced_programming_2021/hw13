#pragma once

class Server;

#include "Server.h"
#include <string>
#include <thread>


enum states{LOGIN_ACCEPT = 1, GENERAL_UPDATE, UPDATE_CHAT, SEND_MESSAGE, OFFLINE};

class ClientHandler
{
public:
	ClientHandler(SOCKET sock, const std::string& name, Server& server);
	~ClientHandler();
	
	

private:

	void handler(Server& server);
	void parsePacket(std::string& new_msg, std::string& second_user);

	SOCKET _client_sock;
	states _client_state;
	std::string _name;
	std::thread _thread;


};
