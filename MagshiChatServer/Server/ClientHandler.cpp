#include "ClientHandler.h"
#include "Helper.h"
#include <chrono>
#include <iostream>

/*
CTOR for ClientHandler object
INPUT: client socket, client username, server object, SOCKET, string, Server object reference
OUTPUT: None
*/

ClientHandler::ClientHandler(SOCKET sock, const std::string& name, Server& server)
{
	this->_client_sock = sock;
	this->_name = name;
	this->_client_state = LOGIN_ACCEPT;
	this->_thread = std::thread(&ClientHandler::handler, this, std::ref(server));

}

/*
DTOR for ClientHandler object
INPUT: None
OUTPUT: None
*/


ClientHandler::~ClientHandler()
{
	this->_thread.detach();
}

/*
Client handler function, responsible for all server-client exchange apart from login
INPUT: server object, Server object reference
OUTPUT: None
*/

void ClientHandler::handler(Server& server)
{
	std::string second_user;
	std::string new_msg;
	std::string chat_content;
	std::string chat_file_name;

	while (this->_client_state != OFFLINE)
	{
		
		if(!second_user.empty()){ chat_file_name = ((second_user < this->_name) ? second_user + "&" + this->_name : this->_name + "&" + second_user) + std::string(".txt"); }

		switch (this->_client_state)
		{
			case SEND_MESSAGE:

				server.addMsg(chat_file_name + "#" + this->_name + "##" + new_msg);
				server.notifyMsg();

			case UPDATE_CHAT:

				try 
				{
					chat_content = server.readChatFile(chat_file_name);
				}
				catch (std::exception& e)
				{
					std::cout << "Error occured while opening chat file with user " << second_user <<": " << e.what() << std::endl;
					this->_client_state = OFFLINE;
					break;
				}
					

			case LOGIN_ACCEPT:

			case GENERAL_UPDATE:

				try
				{
					Helper::send_update_message_to_client(this->_client_sock, chat_content, second_user, server.getAllUsers());
				}
				catch (std::exception& e)
				{
					std::cout << "Error occured while sending response message to user " << second_user << ": " << e.what() << std::endl;
					this->_client_state = OFFLINE;
				}

				break;

			default:
				break;

		}

		if (this->_client_state != OFFLINE)
		{
			this->parsePacket(new_msg, second_user);
		}

	}
	
	std::cout << "Disconnecting " << this->_name << std::endl;
	closesocket(this->_client_sock);
	server.logout(this->_name);
	delete this;
	
}

/*
Function parses 204 message types from client 
INPUT: new message client sent, other user message meant for, 2x string reference
OUTPUT: None
*/

void ClientHandler::parsePacket(std::string& new_msg, std::string& second_user)
{
	int messageTypeCode = 0;

	try 
	{
		if ((messageTypeCode = Helper::getMessageTypeCode(this->_client_sock)) == 204)
		{
			if (!(second_user = Helper::getStringPartFromSocket(this->_client_sock, Helper::getIntPartFromSocket(this->_client_sock, 2))).empty())
			{
				if (!(new_msg = Helper::getStringPartFromSocket(this->_client_sock, Helper::getIntPartFromSocket(this->_client_sock, 5))).empty())
				{
					this->_client_state = SEND_MESSAGE;
				}
				else
				{
					this->_client_state = UPDATE_CHAT;
				}
			}
			else
			{
				this->_client_state = GENERAL_UPDATE;
				Helper::getStringPartFromSocket(this->_client_sock, Helper::getIntPartFromSocket(this->_client_sock, 5));
			}
		}
		else
		{
			std::cout << "Uknown message code recieved from user " << this->_name << ": " << messageTypeCode << std::endl;
			this->_client_state = OFFLINE;
		}
	}
	catch(std::exception& e)
	{
		std::cout << "Undefined behaviour in socket of user " << this->_name << ": " << e.what() << std::endl;
		this->_client_state = OFFLINE;
	}
	
}
