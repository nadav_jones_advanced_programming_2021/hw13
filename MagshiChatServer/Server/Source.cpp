#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;
		 
		myServer.serve(TESTER_PORT);
	}
	catch (std::exception& e)
	{
		std::cout << "Server failure, error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}